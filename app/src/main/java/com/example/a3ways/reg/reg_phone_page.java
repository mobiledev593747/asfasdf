package com.example.a3ways.reg;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.a3ways.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import java.util.concurrent.TimeUnit;

public class reg_phone_page extends AppCompatActivity {
    EditText phone;
    EditText pass;
    Button enter;
    String mobile;
    String password;
    private ProgressDialog progressDialog;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private void initView (){
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks(){
            @Override
            public void onCodeAutoRetrievalTimeOut(String verId){
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
                Toast.makeText(getBaseContext(),"Your Phone Number Verification is failed. Retry again!",Toast.LENGTH_LONG).show();
            }
            @Override
            public void onVerificationCompleted(PhoneAuthCredential cred){
                Log.d("onVerificationCompleted","onVerificationCompleted"+cred);
                if(progressDialog == null){
                    signInWithPhoneAuthCredential(cred);
                }
                else{
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onVerificationFailed(FirebaseException e){
                Log.w("onVerificationFailed", "onVerificationFailed", e);
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
                if(e instanceof FirebaseAuthInvalidCredentialsException){
                    Log.e("Exception","FirebaseAuthInvalidCredentialsException"+ e);
                } else if(e instanceof FirebaseTooManyRequestsException){
                    Log.e("Exception","FirebaseTooManyRequestsException"+ e);
                }
                Toast.makeText(getBaseContext(),"Your Phone Number Verification is failed.Retry again!",Toast.LENGTH_LONG).show();
            }
            @Override
            public void onCodeSent(String verId,PhoneAuthProvider.ForceResendingToken token){
                Log.d("onCodeSent","onCodeSent"+verId);
                Log.i("Ver code",verId);
            }
        };
    }
    public ProgressDialog showProgressDialog(final Context act,final String message, boolean isCancelable) {
        progressDialog = new ProgressDialog(act);
        progressDialog.show();
        progressDialog.setCancelable(isCancelable);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(message);
        return progressDialog;
    }
    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 60, TimeUnit.SECONDS, this, mCallbacks);
    }

    public void onClick(View view){
        mobile = phone.getText().toString();
        password = pass.getText().toString();
        Log.d("Ur phone number",mobile);
        if(mobile != null && !mobile.isEmpty()){
            startPhoneNumberVerification(mobile);
            showProgressDialog(this,"Sending a ver code",false);
        } else{
            showToast("Error");
        }
    }
    public void showToast(String mes){
        Toast.makeText(getBaseContext(),mes,Toast.LENGTH_LONG).show();
    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser user = task.getResult().getUser();
                            Log.d("Sign in with phone auth","Success"+ user);
                        }
                        else{
                            showToast("Error with sign in");
                        }
                    }
                });
    }
    @Override
    protected void onCreate(Bundle saved){
        super.onCreate(saved);
        setContentView(R.layout.reg_phone_page);
        phone = findViewById(R.id.mobile_phone);
        pass = findViewById(R.id.password);
        enter = findViewById(R.id.signin);
        initView();
    }
}
