package com.example.a3ways.reg;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.a3ways.MainActivity;
import com.example.a3ways.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class reg_mail_page extends AppCompatActivity {
    EditText mail;
    EditText pass;
    Button enter;
    String email;
    String password;
    public void onClick(View view){
        email = mail.getText().toString();
        password = pass.getText().toString();
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            Toast.makeText(getBaseContext(), "Регистрация прошла успешно", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getBaseContext(), "Данная почта уже зарегистрирована",
                                    Toast.LENGTH_SHORT).show();
                            Intent main = new Intent(reg_mail_page.this, MainActivity.class);
                            startActivity(main);
                        }
                    }
                });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_mail_page);
        mail = findViewById(R.id.mail);
        pass = findViewById(R.id.password);
        enter= findViewById(R.id.signin);
    }
}
