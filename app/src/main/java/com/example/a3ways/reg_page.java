package com.example.a3ways;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.a3ways.reg.reg_mail_page;
import com.example.a3ways.reg.reg_phone_page;

public class reg_page extends AppCompatActivity {
    public void onClick(View v){
        switch (v.getId()){
            case R.id.mailpass:
                Intent mail = new Intent(reg_page.this, reg_mail_page.class);
                startActivity(mail);
                break;
                /*
            case R.id.userpass:
                Intent name = new Intent(reg_page.this, username_page.class);
                startActivity(name);
                break; */
            case R.id.mobile:
                Intent phone = new Intent(reg_page.this, reg_phone_page.class);
                startActivity(phone);
                break;

        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_act);
    }
}
